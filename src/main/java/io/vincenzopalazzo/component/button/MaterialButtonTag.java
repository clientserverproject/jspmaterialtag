/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.vincenzopalazzo.component.button;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * This tah import style material
 * @author https://github.com/vincenzopalazzo
 */
public class MaterialButtonTag extends TagSupport{

    private String type;
    private String value;

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.println("<button class='matter-button-contained' " + "type='" + type + "' >");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return super.doStartTag();
    }

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            //TODO add ing text for button
            out.print(value);
            out.println("</button>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.doEndTag();
    }
}

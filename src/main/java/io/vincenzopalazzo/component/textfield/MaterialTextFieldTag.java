package io.vincenzopalazzo.component.textfield;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class MaterialTextFieldTag extends TagSupport {

    private String placeholder;
    private String value;
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Guide line library css
     * <label class='matter-textfield-filled'>
     *     <input placeholder=" "/>
     *     <span>Textfield</span>
     * </label>
     */

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();

        try {
            out.println("<label class='matter-textfield-filled'>");
            out.println("<input name='" + name + "' placeholder='" + placeholder +"'/>");
            out.println("<span>" + value + "</span>");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return super.doStartTag();
    }

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();

        try {
            out.println("</label>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.doEndTag();
    }
}

package io.vincenzopalazzo.component.link;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * This tah import a css
 * @author https://github.com/vincenzopalazzo
 */
public class MaterialLinkTag extends TagSupport {

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.println("<link href='https://res.cloudinary.com/finnhvman/raw/upload/matter/matter-0.2.2.css' rel='stylesheet'>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.doStartTag();
    }
}

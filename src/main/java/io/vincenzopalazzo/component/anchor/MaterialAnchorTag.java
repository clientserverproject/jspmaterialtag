package io.vincenzopalazzo.component.anchor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class MaterialAnchorTag extends TagSupport {

    private String href;
    private String value;
    private boolean encodeUrl;

    public void setEncodeUrl(boolean encodeUrl) {
        this.encodeUrl = encodeUrl;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /**
     * The guide line for css project
     * <a class="matter-link" href="https://github.com/finnhvman/matter">Link</a>
     */
    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            if(encodeUrl){
                HttpServletResponse request = (HttpServletResponse) pageContext.getResponse();
                out.println("<a class='matter-link' href='" + request.encodeURL(href)  + "'>");
                return super.doStartTag();
            }
            out.println("<a class='matter-link' href='" + href  + "'>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.doStartTag();
    }

    @Override
    public int doEndTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.println(value + "</a>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.doEndTag();
    }
}
